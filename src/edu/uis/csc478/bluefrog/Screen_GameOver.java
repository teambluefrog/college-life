package edu.uis.csc478.bluefrog;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

import edu.uis.csc478.bluefrog.Window;

/**
 * This class contains the GUI for the Game Over screen.
 * 
 * RQ 3.9.5.1
 */
@SuppressWarnings("serial")
public class Screen_GameOver extends JPanel implements ActionListener
{
    private Game game;
    private Window window;
    private Image background;

    private JLabel dataReason;
    private JButton replayButton, exitButton;

    /**
     * Constructor - Adds components to screen for display.
     * 
     * @param game
     *            The Game instance.
     * 
     * @param window
     *            The Window instance.
     *            
     * @param background
     *            The background image for the window.
     *            
     * RQ 3.9.5.2, RQ 3.9.5.4, RQ 3.9.5.5
     */
    public Screen_GameOver(Game game, Window window, Image background)
    {
        this.game = game;
        this.window = window;
        this.background = background;

        setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        c.insets = new Insets(0, 0, 0, 0);

        // GAME OVER label.
        JLabel labelGameOver = new JLabel("GAME OVER");
        labelGameOver.setFont(window.getFont(Window.FontStyle.CONDENSED_EXTRA_BOLD).deriveFont(72f));
        labelGameOver.setForeground(Window.DARK_BLUE);
        c.gridx = 0;
        c.gridwidth = 2;
        c.gridy = 0;
        c.insets.set(0, 20, 0, 0);
        add(labelGameOver, c);

        // Displays the reason why the game ended.
        dataReason = new JLabel("Congratulations! You Graduated!");
        dataReason.setFont(window.getFont(Window.FontStyle.CONDENSED_EXTRA_BOLD).deriveFont(18f));
        dataReason.setForeground(Window.ORANGE);
        c.gridx = 0;
        c.gridwidth = 2;
        c.gridy = 1;
        c.insets.set(80, 20, 100, 0);
        add(dataReason, c);

        // Play Again button
        replayButton = new JButton("PLAY AGAIN");
        replayButton.addActionListener(this);
        replayButton.setPreferredSize(new Dimension(210, 72));
        replayButton.setFont(window.getFont(Window.FontStyle.CONDENSED_EXTRA_BOLD).deriveFont(34f));
        replayButton.setForeground(Color.WHITE);
        replayButton.setBackground(Window.ORANGE);
        replayButton.setOpaque(true);
        replayButton.setBorder(BorderFactory.createRaisedBevelBorder());
        c.gridx = 0;
        c.gridwidth = 1;
        c.gridy = 2;
        c.insets.set(0, 20, 60, 60);
        c.anchor = GridBagConstraints.PAGE_START;
        add(replayButton, c);

        // Exit button
        exitButton = new JButton("EXIT GAME");
        exitButton.addActionListener(this);
        exitButton.setPreferredSize(new Dimension(210, 72));
        exitButton.setFont(window.getFont(Window.FontStyle.CONDENSED_EXTRA_BOLD).deriveFont(34f));
        exitButton.setForeground(Color.WHITE);
        exitButton.setBackground(Window.ORANGE);
        exitButton.setOpaque(true);
        exitButton.setBorder(BorderFactory.createRaisedBevelBorder());
        c.gridx = 1;
        c.gridwidth = 1;
        c.gridy = 2;
        c.insets.set(0, 0, 0, 0);
        c.anchor = GridBagConstraints.PAGE_START;
        add(exitButton, c);
    }

    /**
     * Sets menu and button link functionality.
     */
    @Override
    public void actionPerformed(ActionEvent event)
    {
        Object source = event.getSource();

        if (source.equals(replayButton))
        {
            game.restart();
        }
        if (source.equals(exitButton))
        {
            window.dispose();
        }
    }

    /**
     * Displays reason game ended to user.
     * 
     * @param reasonGameEnded
     *            String of reason the game ended.
     *            
     * RQ 3.9.5.3
     */
    public void displayGameOver(String reasonGameEnded)
    {
        dataReason.setText(reasonGameEnded);
    }

    /*
     * Original Code by Michael Myers via Stack Overflow
     * 
     * stackoverflow.com/users/13531/michael-myers
     */
    @Override
    protected void paintComponent(Graphics g)
    {
        super.paintComponent(g);
        g.drawImage(background, 0, 0, this);
    }

}
