package edu.uis.csc478.bluefrog;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

import edu.uis.csc478.bluefrog.Window;

/**
 * This class contains the GUI for the Overview screen.
 * 
 * RQ 3.9.3.1, RQ 3.9.3.2, RQ 3.9.3.3, RQ 3.9.3.4
 */
@SuppressWarnings("serial")
public class Screen_Overview extends JPanel implements ActionListener
{
    @SuppressWarnings("unused")
    private Game game;
    private Window window;
    private Image background;

    private final String overviewText = "Your job is to complete four years of college while juggling studies, "
            + "finances, and a social life. You'll be given four turns each semester to choose whether you want "
            + "to spend your time studying, working, or playing. Choose wisely because your decision will have "
            + "consequences. Study too much and you could have to leave college because of lack of funds. "
            + "Play too much and you could be kicked out because of your grades. Or work too much and your "
            + "mood or grades could suffer. In addition to school, you'll also be dealt real life situations, "
            + "such as coming down with mononucleosis, earning a scholarship, or joining the Army Reserves, "
            + "all of which may affect your three primary aspects of your college life. Will you make it "
            + "through all 4 years? Let's find out!!";

    private JButton backButton, manualButton;

    /**
     * Constructor - Adds components to screen for display.
     * 
     * @param game
     *            The Game instance.
     * 
     * @param window
     *            The Window instance.
     *            
     * @param background
     *            The background image for the window.
     */
    public Screen_Overview(Game game, Window window, Image background)
    {
        this.game = game;
        this.window = window;
        this.background = background;

        setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        c.insets = new Insets(0, 0, 0, 0);

        // Add back button.
        backButton = new JButton("Back");
        backButton.addActionListener(this);
        backButton.setPreferredSize(new Dimension(70, 28));
        backButton.setFont(window.getFont(Window.FontStyle.CONDENSED_MEDIUM).deriveFont(14f));
        backButton.setForeground(Window.DARK_BLUE);
        backButton.setBackground(Color.WHITE);
        backButton.setOpaque(true);
        backButton.setBorder(BorderFactory.createLineBorder(Window.DARK_BLUE, 2));
        c.gridx = 0;
        c.gridy = 0;
        c.insets.set(0, 690, 0, 0);
        c.anchor = GridBagConstraints.FIRST_LINE_END;
        add(backButton, c);

        // Adds Game Overview text.
        JLabel labelOverview = new JLabel("GAME OVERVIEW");
        labelOverview.setFont(window.getFont(Window.FontStyle.CONDENSED_EXTRA_BOLD).deriveFont(72f));
        labelOverview.setForeground(Window.DARK_BLUE);
        c.gridx = 0;
        c.gridy = 1;
        c.insets.set(20, 0, 20, 0);
        c.anchor = GridBagConstraints.CENTER;
        add(labelOverview, c);

        // Adds text area that holds the game overview text.
        JTextArea dataOverview = new JTextArea(overviewText);
        dataOverview.setPreferredSize(new Dimension(660, 260));
        dataOverview.setFont(window.getFont(Window.FontStyle.MEDIUM).deriveFont(18f));
        dataOverview.setForeground(Window.DARK_BLUE);
        dataOverview.setBackground(Window.LIGHT_GREEN);
        dataOverview.setLineWrap(true);
        dataOverview.setWrapStyleWord(true);
        dataOverview.setEditable(false);
        dataOverview.setFocusable(false);
        c.gridx = 0;
        c.gridy = 2;
        c.insets.set(0, 10, 0, 0);
        add(dataOverview, c);

        // Adds User's Manual button.
        manualButton = new JButton("USER'S MANUAL (Web)");
        manualButton.addActionListener(this);
        manualButton.setPreferredSize(new Dimension(336, 67));
        manualButton.setFont(window.getFont(Window.FontStyle.CONDENSED_EXTRA_BOLD).deriveFont(23f));
        manualButton.setForeground(Color.WHITE);
        manualButton.setBackground(Window.ORANGE);
        manualButton.setOpaque(true);
        manualButton.setBorder(BorderFactory.createRaisedBevelBorder());
        c.gridx = 0;
        c.gridy = 3;
        c.insets.set(50, 30, 0, 0);
        add(manualButton, c);
    }

    /**
     * Sets link/button destinations.
     */
    @Override
    public void actionPerformed(ActionEvent event)
    {
        Object source = event.getSource();

        if (source.equals(backButton))
        {
            window.changeScreen(Window.ScreenType.START);
        }
        if (source.equals(manualButton))
        {
            try
            {
                Desktop.getDesktop().browse(window.getUserManual());
            } catch (Exception eX)
            {
                System.out.println(eX.getMessage());
            }
        }
    }

    /*
     * Original Code by Michael Myers via Stack Overflow
     * 
     * stackoverflow.com/users/13531/michael-myers
     */
    @Override
    protected void paintComponent(Graphics g)
    {
        super.paintComponent(g);
        g.drawImage(background, 0, 0, this);
    }
}
