package edu.uis.csc478.bluefrog;

/**
 * Events are random curve balls that can be thrown at the player at any time during the game.  They can affect 
 * the play positively or negatively, depending on the event.
 */
public class Event
{
    /**
     * The upper bound of random number for generating the index of a potential event.
     */
    private static final int EVENT_CHANCE_LIMIT = 50;

    /**
     * The Game instance.
     */
    private Game game;
    
    /**
     * The user playing the game.
     */
    private Student player;

    /**
     * Whether the conditions for the specific event to happen have been met or not.
     */
    private boolean conditionsMet = false;
    
    /**
     * The amount to increase the cost of attendance.
     */
    private double coaBonus;
    
    /**
     * The amount to increase the player's funds.
     */
    private double fundsBonus;
    
    /**
     * The amount to increase (positive number) or decrease (negative number) 
     * the player's grade.
     */
    private double gradeBonus;
    
    /**
     * The id number of the event.
     */
    private int index;
    
    /**
     * The number of levels to increase (positive number) or decrease (negative number) 
     * the player's mood.
     */
    private int moodBonus;

    /**
     * The message about the event that gets displayed to the user in the turn results area.
     */
    private String message;
    
    /**
     * The title of the event that gets displayed to the user in the turn results area.
     */
    private String title;

    /**
     * Event Constructor
     * 
     * @param game
     *            The active instance of the game.
     * 
     * @param player
     *            The active instance of the player.
     * 
     * @param eventID
     *            There are 26 possible events. During each turn, a random number
     *            (eventID) is generated between 0 (inclusive) and
     *            event_chance_limit (exclusive). Any random event above the number
     *            of events (less one) will result in a non-event which will have no
     *            effect and the game will continue.
     *            
     * RQ 3.7.2, RQ 3.7.3
     */
    public Event(Game game, Student player, int eventID)
    {
        this.game = game;
        this.player = player;
        index = eventID;
        coaBonus = fundsBonus = gradeBonus = moodBonus = 0;
        initializeEvent();

        // Each event has its own condition(s). If those conditions are not met, the
        // event doesn't happen.
        if (conditionsMet)
        {
            displayEvent();
            applyEvent();
        }
    }

    /**
     * Get the upper limit for generating a random eventID.
     * 
     * @return Returns the upper limit of eventID.
     */
    public static int getEventChanceLimit()
    {
        return EVENT_CHANCE_LIMIT;
    }

    /**
     * Adds the event notification to the information that gets output to the user
     * to notify the user that the event happened and how it affects play.
     * 
     * QR 3.7.4
     */
    private void displayEvent()
    {
        game.setTurnEvent(title);
        game.appendToTurnContent(title + ".\n" + message);
    }

    /**
     * Applies the changes that the event caused to the player's attributes.
     */
    private void applyEvent()
    {
        player.increaseCOA(coaBonus);
        player.increaseFunds(fundsBonus);
        player.increaseGrade(gradeBonus);
        player.increaseMood(moodBonus);
    }

    /**
     * Get's the specific event from the randomly generated eventID. If eventID is
     * higher than the number of events, no event occurs for the turn.
     * 
     * RQ 3.7.5, 
     */
    private void initializeEvent()
    {
        switch (index)
        {
        case 0:
            conditionsMet = true;
            title = "Condolences. Your rich uncle just died.";
            message = "You inherit " + CollegeLife.formatMoney(5000) + ".\n" 
                + "but your mood decreases two levels.";
            fundsBonus = 5000;
            moodBonus = (-2);
            break;
        case 1:
            if (player.getFocus().equals("Play"))
            {
                conditionsMet = true;
                title = "You just became the MVP of your basketball team!";
                message = "Your mood increases one level.";
                moodBonus = 1;
            }
            break;
        case 2:
            conditionsMet = true;
            title = "You" + (char) 39 + "ve just contracted mononeucleosis.";
            message = "You sleep through the month.\n"
                + "Your grades decrease by 1.5 .\n"
                + "Your mood decreases one level.";
            gradeBonus = (-1.5);
            moodBonus = (-1);
            break;
        case 3:
            if (game.getTurnIndex() >= (CollegeLife.TURNS_PER_YEAR * 2))
            {
                int turnsRemaining = CollegeLife.MAX_TURNS - game.getTurnIndex();
                int semestersRemaining = turnsRemaining / CollegeLife.TURNS_PER_SEMESTER;
                int grantAmount = semestersRemaining * 1250;

                conditionsMet = true;
                title = "You" + (char) 39 + "ve just received a grant!";
                message = "You have been awarded a grant\n" + "in the amount of " + grantAmount + ".\n"
                        + "Your tuition will be reduced by $2,500 per semester.";
                coaBonus = 2500;
            }
            break;
        case 4:
            if (player.getMood() == (-3) && game.player.getGPA() < 2.5)
            {
                conditionsMet = true;
                title = "You were just caught cheating.";
                message = "You are being referred to the Dean" + (char) 39 + "s office.";
                game.player.placeOnProbation();
            }
            break;
        case 5:
            if (player.getMood() == (-3) && game.player.getDebt() >= CollegeLife.DEBT_LIMIT)
            {
                conditionsMet = true;
                title = "You were just caught stealing.";
                message = ""; // No message is displayed. Game ends immediately.
                game.endGame("You got caught stealing! You have been expelled.");
            }
            break;
        case 6:
            conditionsMet = true;
            title = "You were hit by a drunk driver.";
            message = "You were hospitalized.\n" 
                + "Your grades decrease by 1.0\n"
                + "Your hospital bill is $3,000.\n"
                + "You mood decreases one level";
            moodBonus = -1;
            gradeBonus = -1.0;
            fundsBonus = -3000;
            break;
        case 7:
            if (player.getMood() < -1)
            {
                conditionsMet = true;
                title = "You consumed too much alcohol at a party.";
                message = "You were hospitalized for alcohol poisoning.\n"
                    + "Your grades decrease by 1.0\n"
                    + "Your hospital bill is $1,000.\n"
                    + "Your mood decreases by 1";
                moodBonus = -1;
                gradeBonus = -1.0;
                fundsBonus = -1000;
            }
            break;
        case 8:
            if (player.getGPA() >= 3.0 && game.player.getMood() >= 0)
            {
                conditionsMet = true;
                title = "You received an internship.";
                message = "You received a short internship in your field of study.\n"
                        + "Your grade increases by 0.5";
                gradeBonus = 0.5;
            }
            break;
        case 9:
            if ((game.getTurnIndex() / CollegeLife.TURNS_PER_SEMESTER) == 3)
            {
                conditionsMet = true;
                title = "You" + (char) 39 + "ve aced your Communications class.";
                message = "You automatically get an A on the final";
                gradeBonus = 4.0; // Grade above 4 will be reduced to 4.0
            }
            break;
        case 10:
            conditionsMet = true;
            title = "You made friends!";
            message = "You just met some new and interesting people at a sports event.\n"
                + "Your mood increases one level";
            break;
        case 11:
            conditionsMet = true;
            title = "You hosted a successful study session.";
            message = "Studying with others paid off.\n"
                + "Your grades increase by 0.2\n"
                + "Your mood increases one level";
            gradeBonus = 0.2;
            moodBonus = 1;
            break;
        case 12:
            conditionsMet = true;
            title = "You just threw an amazing party!";
            message = "Your secret party is a big hit.\n"
                + "Your mood increases two levels";
            moodBonus = 2;
            break;
        case 13:
            conditionsMet = true;
            title = "You just got laid off from your job.";
            message = "You" + (char) 39 + "ve lost your part-time job.\n"
                    + "Selecting " + (char) 39 + "Work" + (char) 39 + " pays half-as much for the rest of the semester.\n" + "Your mood decreases by 1";
            game.player.setIsEmployed(false);
            break;
        case 14:
            if (player.getFocus() == "Play")
            {
                conditionsMet = true;
                title = "You just hosted a successful charity event.";
                message = "Your used your free time to benefit others.\n"
                    + "Your mood increases two levels instead of one";
                moodBonus = 1; // +1 already granted by Play button
            }
            break;
        case 15:
            conditionsMet = true;
            title = "You just found out you" + (char) 39 + "re pregnant.";
            message = "You" + (char) 39 + "re going to be a parent!\n"
                + "Your grades decrease by 1.0\n"
                + "Your mood decreases one level";
            gradeBonus = -1.0;
            moodBonus = -1;
            break;
        case 16:
            if (player.getFocus() == "Play")
            {
                conditionsMet = true;
                title = "You" + (char) 39 + "re dealing with an ugly hangover.";
                message = "You played just a little too hard.\n"
                    + "Your grades decrease by 0.1";
                gradeBonus = -0.1;
            }
            break;
        case 17:
            conditionsMet = true;
            title = "A family member passed away.";
            message = "Your grades decrease by 0.3\n"
                + "Your mood decreases two levels";
            gradeBonus = -0.3;
            moodBonus = -2;
            break;
        case 18:
            conditionsMet = true;
            title = "A family member passed away.";
            message = "You need to cover funeral expenses\n"
                    + "Your grades decrease by 0.3\n"
                    + "Your funds decrease by $3.000\n"
                    + "Your mood decreases two levels";
            break;
        case 19:
            if (player.getFocus() == "Play" && player.getGPA() >= 3.2)
            {
                conditionsMet = true;
                title = "You" + (char) 39 + "ve just been accepted into your fraternity/sorority of choice.";
                message = "Membership dues are required"
                        + "Your funds decrease by $250\n"
                        + "Your mood increases by two levels instead of one";
                fundsBonus = -250;
                moodBonus = 1; // +1 already granted by Play button
            }
            break;
        case 20:
            if (player.getFocus() == "Play")
            {
                conditionsMet = true;
                title = "You made it onto the traveling swim team.";
                message = "Your mood increases by two levels instead of one";
                moodBonus = 1; // +1 already granted by Play button
            }
            break;
        case 21:
            conditionsMet = true;
            title = "You came down with a serious illness.";
            message = "You were hospitalized.\n"
                + "Your grades decrease by 0.5\n"
                + "Your hospital bill is $5,000\n"
                + "Your mood decreases one level";
            moodBonus = 1;
            gradeBonus = -0.5;
            fundsBonus = -5000;
            break;
        case 22:
            if (player.getGPA() < 2.2 && player.getDebt() >= (CollegeLife.DEBT_LIMIT - CollegeLife.LOAN_AMOUNT)
                    && player.getMood() < 0)
            {
                conditionsMet = true;
                title = "You had a mental breakdown.";
                message = "The stress of school, work and life in general became too much.\n"
                    + "Your grades decrease by 1.0"
                    + "Your mood drops completely\n";
                gradeBonus = -1.0;
                moodBonus = -3; // Mood below -3 will stay at -3
            }
            break;
        case 23:
            if (player.getFocus() == "Play" && player.getGPA() >= 3.2 && player.getMood() < 0)
            {
                conditionsMet = true;
                title = "You pledged a fraternity/sorority but were not accepted.";
                message = "Your days of pledging are over.\n"
                    + "Your mood decreases one level";
                moodBonus = -1;
            }
            break;
        case 24:
            if (player.getFocus() == "Play")
            {
                conditionsMet = true;
                title = "You were arrested!";
                message = "You attended a fraternity/sorority party that was awesome... "
                    + "until the police showed up and you got arrested\n"
                    + "Your grades decrease by 1.0\n"
                    + "Your funds decrease by $500 for bail\n"
                    + "Your mood decreases by 2";
                moodBonus = -2;
                gradeBonus = 1.0;
                fundsBonus = -250;
            }
            break;
        case 25:
            conditionsMet = true;
            title = "You joined the Army Reserves. Hoo-ah!";
            message = "You join the Army Reservers.\n"
                    + "Your grades decrease by 0.1\n"
                    + "Your income increases by $250 per month.";
            gradeBonus = -0.1;
            game.player.enlistInArmy();
            break;
        default:
            // System.out.println("Event " + index + " is unassigned."); //For testing
            // purposes only
        }
    }
}
