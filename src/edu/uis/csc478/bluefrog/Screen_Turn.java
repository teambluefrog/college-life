package edu.uis.csc478.bluefrog;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

import edu.uis.csc478.bluefrog.Window;

/**
 * This class contains the GUI for the Turn screen.
 * 
 * RQ 3.9.4.1, RQ 3.9.4.2
 */
@SuppressWarnings("serial")
public class Screen_Turn extends JPanel implements ActionListener
{
    private Game game;
    @SuppressWarnings("unused")
    private Window window;
    private Image background;

    private JButton playButton, studyButton, workButton;
    private JLabel dataGrade, dataGPA, dataFunds, dataMood, dataDebt;
    private JLabel dataYear, dataSemester, dataTurnOfSemester, dataTurnOfGame;
    private JLabel labelResults;
    private JTextArea dataResults;

    private ImageIcon[] smiley;

    /**
     * Constructor - Adds components to screen for display.
     * 
     * @param game
     *            The Game instance.
     * 
     * @param window
     *            The Window instance.
     *            
     * @param background
     *            The background image for the window.
     *            
     * @param moodFaces
     *            Array of emoticons to indicate mood.
     *            
     * RQ 3.2.1, RQ 3.3.1.1
     */
    public Screen_Turn(Game game, Window window, Image background, ImageIcon[] moodFaces)
    {
        this.game = game;
        this.window = window;
        this.background = background;
        smiley = moodFaces;

        

        setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        c.insets = new Insets(0, 0, 0, 0);

        Dimension buttonDim = new Dimension(170, 72);
        Dimension sectionDim = new Dimension(180, 30);
        Dimension labelDim = new Dimension(120, 24);
        Dimension dataDim = new Dimension(60, 24);

        Font buttonFont = window.getFont(Window.FontStyle.CONDENSED_EXTRA_BOLD).deriveFont(26f);
        Font sectionFont = window.getFont(Window.FontStyle.BOLD).deriveFont(18f);
        Font labelFont = window.getFont(Window.FontStyle.BOLD).deriveFont(12f);
        Font dataFont = window.getFont(Window.FontStyle.MEDIUM).deriveFont(12f);

        // WORK button.
        workButton = new JButton("WORK");
        workButton.addActionListener(this);
        workButton.setPreferredSize(buttonDim);
        workButton.setFont(buttonFont);
        workButton.setForeground(Color.WHITE);
        workButton.setBackground(Window.ORANGE);
        workButton.setOpaque(true);
        workButton.setBorder(BorderFactory.createRaisedBevelBorder());
        c.gridx = 0;
        c.gridy = 0;
        c.insets.set(110, 50, 0, 0);
        c.anchor = GridBagConstraints.LAST_LINE_END;
        add(workButton, c);

        // STUDY button.
        studyButton = new JButton("STUDY");
        studyButton.addActionListener(this);
        studyButton.setPreferredSize(buttonDim);
        studyButton.setFont(buttonFont);
        studyButton.setForeground(Color.WHITE);
        studyButton.setBackground(Window.ORANGE);
        studyButton.setOpaque(true);
        studyButton.setBorder(BorderFactory.createRaisedBevelBorder());
        c.gridx = 1;
        c.gridy = 0;
        c.insets.set(0, 40, 0, 40);
        c.anchor = GridBagConstraints.PAGE_END;
        add(studyButton, c);

        // PLAY button.
        playButton = new JButton("PLAY");
        playButton.addActionListener(this);
        playButton.setPreferredSize(buttonDim);
        playButton.setFont(buttonFont);
        playButton.setForeground(Color.WHITE);
        playButton.setBackground(Window.ORANGE);
        playButton.setOpaque(true);
        playButton.setBorder(BorderFactory.createRaisedBevelBorder());
        c.gridx = 2;
        c.gridy = 0;
        c.insets.set(0, 0, 0, 0);
        c.anchor = GridBagConstraints.LAST_LINE_START;
        add(playButton, c);

        // Current Status Section
        c.anchor = GridBagConstraints.LINE_START;

        JLabel status = new JLabel("CURRENT STATUS");
        status.setPreferredSize(sectionDim);
        status.setFont(sectionFont);
        status.setForeground(Window.ORANGE);
        c.gridx = 0;
        c.gridy = 1;
        c.insets.set(55, 20, 0, 0);
        add(status, c);

        JLabel labelGrade = new JLabel("Semester Grade:");
        labelGrade.setPreferredSize(labelDim);
        labelGrade.setFont(labelFont);
        c.gridx = 0;
        c.gridy = 2;
        c.insets.set(0, 20, 0, 0);
        add(labelGrade, c);

        JLabel labelGPA = new JLabel("GPA:");
        labelGPA.setPreferredSize(labelDim);
        labelGPA.setFont(labelFont);
        c.gridx = 0;
        c.gridy = 3;
        c.insets.set(0, 20, 0, 0);
        add(labelGPA, c);

        JLabel labelFunds = new JLabel("Finances:");
        labelFunds.setPreferredSize(labelDim);
        labelFunds.setFont(labelFont);
        c.gridx = 0;
        c.gridy = 4;
        c.insets.set(0, 20, 0, 0);
        add(labelFunds, c);

        JLabel labelMood = new JLabel("Mood:");
        labelMood.setPreferredSize(labelDim);
        labelMood.setFont(labelFont);
        c.gridx = 0;
        c.gridy = 5;
        c.insets.set(0, 20, 0, 0);
        add(labelMood, c);

        JLabel labelDebt = new JLabel("Debt:");
        labelDebt.setPreferredSize(labelDim);
        labelDebt.setFont(labelFont);
        c.gridx = 0;
        c.gridy = 6;
        c.insets.set(0, 20, 0, 0);
        add(labelDebt, c);

        dataGrade = new JLabel("N/A");
        dataGrade.setPreferredSize(dataDim);
        dataGrade.setFont(dataFont);
        c.gridx = 0;
        c.gridy = 2;
        c.insets.set(0, 130, 0, 0);
        add(dataGrade, c);

        dataGPA = new JLabel("N/A");
        dataGPA.setPreferredSize(dataDim);
        dataGPA.setFont(dataFont);
        c.gridx = 0;
        c.gridy = 3;
        c.insets.set(0, 60, 0, 0);
        add(dataGPA, c);

        dataFunds = new JLabel("$0");
        dataFunds.setPreferredSize(new Dimension(80, 24));
        dataFunds.setFont(dataFont);
        c.gridx = 0;
        c.gridy = 4;
        c.insets.set(0, 80, 0, 0);
        add(dataFunds, c);

        dataMood = new JLabel(smiley[3]);
        dataMood.setPreferredSize(dataDim);
        dataMood.setFont(dataFont);
        c.gridx = 0;
        c.gridy = 5;
        c.insets.set(0, 50, 0, 0);
        add(dataMood, c);

        dataDebt = new JLabel("$25,000 of $25,000");
        dataDebt.setPreferredSize(new Dimension(120, 24));
        dataDebt.setFont(dataFont);
        c.gridx = 0;
        c.gridy = 6;
        c.insets.set(0, 60, 0, 0);
        add(dataDebt, c);

        // Game Progress section
        JLabel progress = new JLabel("GAME PROGRESS");
        progress.setPreferredSize(sectionDim);
        progress.setFont(sectionFont);
        progress.setForeground(Window.ORANGE);
        c.gridx = 0;
        c.gridy = 7;
        c.insets.set(27, 20, 0, 0);
        add(progress, c);

        JLabel labelYear = new JLabel("Year:");
        labelYear.setPreferredSize(labelDim);
        labelYear.setFont(labelFont);
        c.gridx = 0;
        c.gridy = 8;
        c.insets.set(0, 20, 0, 0);
        add(labelYear, c);

        JLabel labelSemester = new JLabel("Semester:");
        labelSemester.setPreferredSize(labelDim);
        labelSemester.setFont(labelFont);
        c.gridx = 0;
        c.gridy = 9;
        c.insets.set(0, 20, 0, 0);
        add(labelSemester, c);

        dataYear = new JLabel("Sophomore");
        dataYear.setPreferredSize(new Dimension(100, 24));
        dataYear.setFont(dataFont);
        c.gridx = 0;
        c.gridy = 8;
        c.insets.set(0, 60, 0, 0);
        add(dataYear, c);

        dataSemester = new JLabel("Fall");
        dataSemester.setPreferredSize(labelDim);
        dataSemester.setFont(dataFont);
        c.gridx = 0;
        c.gridy = 9;
        c.insets.set(0, 90, 0, 0);
        add(dataSemester, c);

        dataTurnOfSemester = new JLabel("Turn 3 of 4 in semester");
        dataTurnOfSemester.setPreferredSize(new Dimension(180, 24));
        dataTurnOfSemester.setFont(dataFont);
        c.gridx = 0;
        c.gridy = 10;
        c.insets.set(0, 20, 0, 0);
        add(dataTurnOfSemester, c);

        dataTurnOfGame = new JLabel("Turn 11 of 32 in game");
        dataTurnOfGame.setPreferredSize(new Dimension(180, 24));
        dataTurnOfGame.setFont(dataFont);
        c.gridx = 0;
        c.gridy = 11;
        c.insets.set(0, 20, 0, 0);
        add(dataTurnOfGame, c);

        // Turn Results section
        labelResults = new JLabel("TURN 1");
        labelResults.setPreferredSize(new Dimension(400, 30));
        labelResults.setFont(labelFont.deriveFont(18f));
        labelResults.setForeground(Color.WHITE);
        c.gridx = 1;
        c.gridwidth = 2;
        c.gridy = 2;
        c.gridheight = 2;
        c.insets.set(0, 50, 0, 0);
        add(labelResults, c);

        dataResults = new JTextArea();
        JScrollPane dataResultScroller = new JScrollPane(dataResults, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
                JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        dataResultScroller.setPreferredSize(new Dimension(400, 180));
        dataResultScroller.setBorder(BorderFactory.createEmptyBorder());
        dataResults.setFont(dataFont.deriveFont(16f));
        dataResults.setForeground(Color.WHITE);
        dataResults.setBackground(Window.DARK_BLUE);
        dataResults.setLineWrap(true);
        dataResults.setWrapStyleWord(true);
        dataResults.setEditable(false);
        dataResults.setFocusable(false);
        c.gridx = 1;
        c.gridwidth = 2;
        c.gridy = 4;
        c.gridheight = 8;
        c.insets.set(0, 50, 0, 0);
        c.anchor = GridBagConstraints.FIRST_LINE_START;
        add(dataResultScroller, c);
    }

    /**
     * Sets button functionality.
     */
    @Override
    public void actionPerformed(ActionEvent event)
    {
        Object source = event.getSource();

        if (source == workButton)
        {
            game.chooseFocus(CollegeLife.Focus.WORK);
        }
        if (source == studyButton)
        {
            game.chooseFocus(CollegeLife.Focus.STUDY);
        }
        if (source == playButton)
        {
            game.chooseFocus(CollegeLife.Focus.PLAY);
        }
    }

    /**
     * Displays data for current turn.
     * 
     * @param turnIndex
     *            The turn index.
     *            
     * RQ 3.1.3, RQ 3.1.4, RQ 3.6.10
     */
    public void displayCurrentTurn(int turnIndex)
    {
        dataTurnOfGame.setText("Turn " + (turnIndex + 1) + " of " + CollegeLife.MAX_TURNS);

        String displayGPA, displaySemesterGrade;

        if (turnIndex >= CollegeLife.TURNS_PER_SEMESTER)
        {
            displayGPA = Double.toString(game.player.getGPA());
        } else
        {
            displayGPA = displaySemesterGrade = "N/A";
        }
        
        if (turnIndex > 0)
        {
            displaySemesterGrade = CollegeLife.convertToLetterGrade(game.player.getSemesterGrade());
        } else
        {
            displaySemesterGrade = "N/A";
        }
        
        
        dataGrade.setText(displaySemesterGrade);
        dataGPA.setText(displayGPA);
        dataFunds.setText(CollegeLife.formatMoney(game.player.getFunds()));
        dataMood.setIcon(getMoodIcon(game.player.getMood()));
        dataDebt.setText(CollegeLife.formatMoney(game.player.getDebt()) + " of $25,000");

        dataYear.setText(game.getYear());
        dataSemester.setText(game.getSemester());

        dataTurnOfSemester.setText("Turn " + ((turnIndex % CollegeLife.TURNS_PER_SEMESTER) + 1) + " of "
                + (CollegeLife.TURNS_PER_SEMESTER) + " in semester");
    }

    /**
     * Displays results of the previous turn.
     * 
     * @param turn
     *            Turn index.
     * 
     * @param focus
     *            The button the user clicked.
     * 
     * @param moodEffects
     *            The change in mood.
     * 
     * @param gradeEffects
     *            The change in grade.
     * 
     * @param financeEffects
     *            The change in finances.
     * 
     * @param billingStatement
     *            Notice that the player's cost of attendance was paid.
     * 
     * @param gameEvent
     *            Title, details, and effects of a life event if one took place.
     * 
     * @param additionalContent
     *            Any additional messages to the player about the previous turn.
     *            
     * RQ 3.9.4.3, RQ 3.9.4.4, RQ 3.9.4.5
     */
    public void displayResults(int turn, String focus, String moodEffects, String gradeEffects, String financeEffects,
            String billingStatement, String gameEvent, String additionalContent)
    {
        if(game.getTurnIndex() % CollegeLife.TURNS_PER_SEMESTER == CollegeLife.TURNS_PER_SEMESTER - 1)
        {
            gradeEffects += " on your finals";
        }
        
        String turnResults = "You chose " + focus + ".\n" + gameEvent + billingStatement + "As a result, your mood "
                + moodEffects + ", " + "your grades " + gradeEffects + ", and your funds " + financeEffects + ".";

        labelResults.setText("TURN " + (turn + 1) + " RESULTS");
        dataResults.setText(turnResults + additionalContent);
        dataResults.setCaretPosition(0);
    }

    /**
     * Displays different text in turn results header for the first turn.
     */
    public void initializeDisplay()
    {
        labelResults.setText("SELECT YOUR FOCUS FOR THIS TURN");
        dataResults.setText("");
    }

    /**
     * Getter for mood icon.
     * 
     * @param mood
     *            Number equivalent of mood.
     * 
     * @return Returns smiley icon file name.
     */
    private Icon getMoodIcon(int mood)
    {
        Icon result = smiley[mood + 3];

        return result;
    }

    /*
     * Original Code by Michael Myers via Stack Overflow
     * 
     * stackoverflow.com/users/13531/michael-myers
     */
    @Override
    protected void paintComponent(Graphics g)
    {
        super.paintComponent(g);
        g.drawImage(background, 0, 0, this);
    }
}
