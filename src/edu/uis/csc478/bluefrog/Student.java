package edu.uis.csc478.bluefrog;

import javax.swing.*;

public class Student
{
    /**
     * The Game instance.
     */
    private Game game;

    /**
     * The button the user clicked during the turn.
     */
    private CollegeLife.Focus focus;

    /**
     * An array of the player's grades accumulated through the game - 1 per turn.
     */
    private double[] grade = new double[CollegeLife.MAX_TURNS];
    
    /**
     * The amount to increase (positive number) or decrease (negative number) 
     * the player's grade.
     */
    private double gradeBonus;
    
    /**
     * The player's GPA.
     */
    private double  gradePointAverage;
    
    /**
     * The Player's semester grade.
     */
    private double  semesterGrade;
    
    /**
     * The cost of attendance. 
     */
    private int coa;
    
    /**
     * Player's amount of debt.
     */
    private int debt;
    
    /**
     * The amount of money the player has.
     */
    private int funds;
    
    /**
     * The player's mood level.
     */
    private int mood;
    
    /**
     * Used in calculating player's current funds.
     */
    private int previousFunds;
    
    /**
     * Used in calculating player's current mood level.
     */
    private int previousMood;
    
    /**
     * Whether the player is employed.
     */
    private boolean isEmployed;
    
    /**
     * Whether the player is on academic probation.
     */
    private boolean isOnAcademicProbation;
    
    /**
     * Whether the player has enough to cover the cost of attendance.
     */
    private boolean isLowOnFunds;
    
    /**
     * Whether the player is in the Army Reserves.
     */
    private boolean isReservist;

    /**
     * Holds a message to the player to alert them of a change in grade.
     */
    private String changeInGrade;
    
    /**
     * Holds a message to the player to alert them of a change in funds.
     */
    private String changeInFunds;
    
    /**
     * Holds a message to the player to alert them of a change in mood.
     */
    private String changeInMood;

    /**
     * Constructor for Student object. Sets the player's starting attributes.
     * 
     * @param game
     *            An initialized game object which holds all the game info such as
     *            turn, semester, and year and initializes the game and advances the
     *            turn.
     */
    public Student(Game game)
    {
        this.game = game;
        initialize();
    }

    /**
     * If the "Joined the Army" has occurred, isReservist property is set to true.
     */
    public void enlistInArmy()
    {
        this.isReservist = true;
    }

    /**
     * Gets the String that holds a notice to the player of a change in grade.
     * 
     * @return Returns a String notification of a change to aplayer's grade.
     * 
     */
    public String getChangeInGrade()
    {
        return changeInGrade;
    }

    /**
     * Getter for the Cost of Attendance game attribute.
     * 
     * @return Returns an int of the cost of attendance.
     */
    public double getCOA()
    {
        return coa;
    }

    /**
     * Getter for the player's current amount of debt.
     * 
     * @return Returns an int of the player's debt amount.
     */
    public double getDebt()
    {
        return debt;
    }

    /**
     * Determines what player attributes will change based on what button the player
     * clicked.
     * 
     * @param focus
     *            The button the player clicked.
     * 
     * @return Returns a String of what player attributes have changed for display
     *         to the user.
     */
    public String getEffects(CollegeLife.Focus focus)
    {
        String r = null;

        switch (focus)
        {
        case STUDY:
            r = changeInGrade;
            break;
        case WORK:
            r = changeInFunds;
            break;
        case PLAY:
            r = changeInMood;
            break;
        default:
            r = "Error";
        }

        return r;
    }

    /**
     * Getter for isEmployed.
     * 
     * @return Returns a boolean of whether the player is employed or not.
     */
    public boolean getIsEmployed()
    {
        return isEmployed;
    }

    /**
     * Gets a string equivalent of the button the player clicked.
     * 
     * @return Returns the String name of the button the player clicked.
     */
    public String getFocus()
    {
        String r = null;

        switch (focus)
        {
        case STUDY:
            r = "Study";
            break;
        case WORK:
            r = "Work";
            break;
        case PLAY:
            r = "Play";
        }

        return r;
    }

    /**
     * Getter for the player's funds.
     * 
     * @return returns an int of the player's funds.
     */
    public int getFunds()
    {
        return funds;
    }

    /**
     * Getter for the player's GPA.
     * 
     * @return returns a double of the player's GPA.
     */
    public double getGPA()
    {
        return gradePointAverage;
    }

    /**
     * Getter for the player's grade.
     * 
     * @param turnReference
     *            Turn number.
     * 
     * @return Returns a String of the player's grade.
     */
    public double getGrade(int turnReference)
    {
        return grade[turnReference];
    }

    public double getGradeBonus()
    {
        return gradeBonus;
    }

    /**
     * Getter for the player's mood.
     * 
     * @return returns an int of the player's mood.
     */
    public int getMood()
    {
        return mood;
    }

    /**
     * Getter for the player's semester grade.
     * 
     * @return returns an int of the player's semester grade.
     */
    public double getSemesterGrade()
    {
        return semesterGrade;
    }

    /**
     * Increases cost of attendance by provided amount.
     * 
     * @param coaIncrease
     *            Amount the cost of attendance is increased by. Amount can be
     *            negative which would decrease the cost of attendance.
     */
    public void increaseCOA(double coaIncrease)
    {
        coa += coaIncrease;
    }

    /**
     * Increases player's finances by provided amount.
     * 
     * @param fundsIncrease
     *            Amount the player's finances are increased by. Amount can be
     *            negative which would decrease the player's finances.
     *            
     * RQ 3.5.5
     */
    public void increaseFunds(double fundsIncrease)
    {
        funds += fundsIncrease;

        if (funds < 0)
        {
            funds = 0;
        }
    }

    /**
     * Increases a players grade by provided amount.
     * 
     * @param gradeIncrease
     *            Amount the player's grade is increased by. Amount can be negative
     *            which would decrease the player's grade.
     */
    public void increaseGrade(double gradeIncrease)
    {
        gradeBonus += gradeIncrease;
    }

    /**
     * Increases a player's mood by provided amount.
     * 
     * @param moodIncrease
     *            Amount the player's mood is increased by. Amount can be negative
     *            which would decrease the player's mood.
     */
    public void increaseMood(int moodIncrease)
    {
        mood += moodIncrease;
    }

    /**
     * Sets the player's starting defaults.
     */
    public void initialize()
    {
        isEmployed = true;
        isOnAcademicProbation = false;
        isLowOnFunds = false;
        isReservist = false;
        coa = CollegeLife.COST_OF_ATTENDANCE;
        debt = 0;
        funds = previousFunds = CollegeLife.STARTING_FUNDS;
        mood = previousMood = CollegeLife.STARTING_MOOD;

        for (int i = 0; i < grade.length; i++)
        {
            grade[i] = 0;
        }
    }

    /**
     * Puts a player on academic probation.
     */
    public void placeOnProbation()
    {
        isOnAcademicProbation = true;
    }

    /**
     * Player cannot pay for the semester cost of attendance and has requested a
     * loan. Loan amount is added to the player's funds and recorded in the players
     * debt. Notices of amounts and how the loan has affected the player's
     * attributes are added to the turn output.
     * 
     * @param isForCOA
     *            True if the player requested a loan.
     *            
     * RQ 3.6.2, RQ 3.6.6, RQ 3.6.7, RQ 3.6.9
     */
    public void requestLoan(boolean isForCOA)
    {
        if (debt >= CollegeLife.DEBT_LIMIT)
        {
            return;
        }
        
        int choice;
        
        String offer;
        
        if (isForCOA)
        {
            offer = "You have " + CollegeLife.formatMoney(funds) + " in your funds.\n"
                    + "You require " + CollegeLife.formatMoney(coa)
                    + " to pay for the semester.\n\n"
                    + "You can take out a student loan in the amount of "
                    + CollegeLife.formatMoney(CollegeLife.LOAN_AMOUNT) + ".\n"
                    + "You" + (char) 39 + "ll need the loan to continue.";
            
            Object[] options = {"Accept Loan"};
            
            choice = JOptionPane.showOptionDialog(game.mainWindow,
                    offer,
                    "Insufficient Funds",
                    JOptionPane.OK_OPTION,
                    JOptionPane.QUESTION_MESSAGE,
                    null,
                    options,
                    options[0]);
        }
        else
        {
            offer = "You have " + CollegeLife.formatMoney(funds) + " in your funds.\n"
                    + "Play requires " + CollegeLife.formatMoney(250) + ".\n"
                    + "Do you want to take out a student loan in the amount of "
                    + CollegeLife.formatMoney(CollegeLife.LOAN_AMOUNT) + "\n"
                    + "to help cover your personal expenses?";
            
            Object[] options = {"Accept", "Decline"};
            
            choice = JOptionPane.showOptionDialog(game.mainWindow,
                    offer,
                    "Insufficient Funds",
                    JOptionPane.YES_NO_OPTION,
                    JOptionPane.QUESTION_MESSAGE,
                    null,
                    options,
                    options[0]);
        }
        
        if (choice == JOptionPane.YES_OPTION)
        {
            funds += CollegeLife.LOAN_AMOUNT;
            debt += CollegeLife.LOAN_AMOUNT;
            
            game.setTurnEvent("You" + (char) 39 + "ve accepted a student loan.");
            String acceptanceMessage = "You" + (char) 39 + "ve received a student loan in the amount of "
                    + CollegeLife.formatMoney(CollegeLife.LOAN_AMOUNT) + ".\n"
                    + "Your loans now total " + CollegeLife.formatMoney(debt)
                    + " out of a max of " + CollegeLife.formatMoney(CollegeLife.DEBT_LIMIT) + ".";
            game.appendToTurnContent(acceptanceMessage);
        }
    }

    /**
     * Resets the grade bonus to 0.
     */
    public void resetGradeBonus()
    {
        gradeBonus = 0;
    }

    /**
     * Sets the player's focus for the turn according to what button the player
     * clicked.
     * 
     * @param focus
     *            The button the player clicked.
     */
    public void setFocus(CollegeLife.Focus focus)
    {
        this.focus = focus;
    }

    /**
     * Adds a grade to player's grade array and calculates a player's GPA if it's
     * the last turn of the semester.
     * 
     * @param turnIndex
     *            The turn number.
     * 
     * @param turnGrade
     *            The grade the player received for the turn.
     *            
     * RQ 3.3.1.2
     * 
     */
    public void setGrade(int turnIndex, double turnGrade)
    {
        grade[turnIndex] = turnGrade;
        calculateGPA(turnIndex);
    }

    /**
     * Setter for isEmployed attribute.
     * 
     * @param employmentStatus
     *            Player's state of employment.
     */
    public void setIsEmployed(boolean employmentStatus)
    {
        this.isEmployed = employmentStatus;
    }

    /**
     * Checks whether a player has enough funds to pay for cost of attendance and
     * warns them if they don't.
     * 
     * RQ 3.5.6
     */
    public void checkFunds()
    {
        if (funds < coa && !isLowOnFunds)
        {
            isLowOnFunds = true;
            if (game.getTurnIndex() < (CollegeLife.MAX_TURNS - 1))
            {
                String warning = "Your finances are low!\n"
                        + "You can" + (char) 39 + "t pay for the next semester yet. Consider working more or "
                        + "you won" + (char) 39 + "t be coming back next semester!";
                game.appendToTurnContent(warning);
            }
        } else
        {
            isLowOnFunds = false;
        }
    }

    /**
     * Notifies whether a player has found employment or is still looking during the
     * fourth turn of the semester. Notifies soldiers that they've received their
     * paycheck.
     */
    public void checkEmployment()
    {
        if (!isEmployed)
        {
            // If it's the fourth turn of the semester.
            if (game.getTurnIndex() % CollegeLife.TURNS_PER_SEMESTER == 3)
            {
                game.appendToTurnContent("You found another part-time job!");
                isEmployed = true;
            } else
            {
                game.appendToTurnContent("You" + (char) 39 + "re still looking for work");
            }

        }

        if (isReservist)
        {
            game.appendToTurnContent("You receive Duty Pay of $250");
            this.increaseFunds(0.25);
        }
    }

    /**
     * Calculates and notifies player to changes in player attributes - grade, mood,
     * and funds.
     * 
     * @param turnIndex
     *            The current turn.
     *            
     * RQ 3.2.3, RQ 3.4.1
     */
    public void calculateStatusChange(int turnIndex)
    {
        /*
         * Compare current grade to previous grade
         */

        String currentLetterGrade, previousLetterGrade;

        currentLetterGrade = CollegeLife.convertToLetterGrade(grade[turnIndex]);

        // If it's the first turn of the semester.
        if ((turnIndex % CollegeLife.TURNS_PER_SEMESTER) == 0)
        {
            changeInGrade = "start the semester at " + currentLetterGrade;
        } else
        {
            previousLetterGrade = CollegeLife.convertToLetterGrade(grade[turnIndex - 1]);

            int change = CollegeLife.compareLetterGrades(currentLetterGrade, previousLetterGrade);

            if (change > 0)
            {
                changeInGrade = "decreased to " + currentLetterGrade;
            } else if (change < 0)
            {
                changeInGrade = "increased to " + currentLetterGrade;
            } else
            {
                changeInGrade = "stayed at " + currentLetterGrade;
            }
        }

        /*
         * Compare current mood to previous mood
         */

        if (mood > 3)
        {
            mood = 3;
        } else if (mood < -3)
        {
            mood = -3;
        }
        
        if (mood < 0)
        {
            gradeBonus += (mood * 0.25); // Grade decreased for negative mood
        }

        int moodIncrease = mood - previousMood;

        if (moodIncrease > 0)
        {
            changeInMood = "increased by " + moodIncrease + " levels";
        } else if (moodIncrease < 0)
        {
            changeInMood = "decreased by " + (moodIncrease * -1) + " levels";
        } else
        {
            changeInMood = "is unchanged";
        }

        // Set previousMood to current mood for next turn.
        previousMood = mood;

        /*
         * Compare current funds to previous funds
         */

        if (funds < 0)
        {
            funds = 0;
        }

        int fundsIncrease = funds - previousFunds;

        if (fundsIncrease > 0)
        {
            changeInFunds = "increased by " + CollegeLife.formatMoney(fundsIncrease);
        } else if (fundsIncrease < 0)
        {
            changeInFunds = "decreased by " + CollegeLife.formatMoney(fundsIncrease * -1);
        } else
        {
            changeInFunds = "stayed at " + CollegeLife.formatMoney(getFunds());
        }

        // Set previousFunds to current funds for next turn
        previousFunds = funds;
    }

    /**
     * Calculates the player's GPA.
     * 
     * @param turnIndex
     *            The turn index.
     *            
     * RQ 3.3.1.3
     */
    private void calculateGPA(int turnIndex)
    {
        double gradeCount, gradeTotal, semesterCount, semesterTotal;
        gradeCount = gradeTotal = semesterCount = semesterTotal = 0.0;

        int finalsWeek = CollegeLife.TURNS_PER_SEMESTER - 1;

        for (int i = 0; i <= turnIndex; i++)
        {
            // If it's the fourth turn of the semester.
            if (i % CollegeLife.TURNS_PER_SEMESTER == finalsWeek)
            {
                double weightedGrade = grade[i] * CollegeLife.FINALS_WEIGHT;
                gradeTotal += weightedGrade;
                gradeCount += CollegeLife.FINALS_WEIGHT;

                semesterTotal += weightedGrade;
                semesterCount += CollegeLife.FINALS_WEIGHT;

                // After semester grade is stored, variables are reinitialized for following
                // semester
                semesterGrade = CollegeLife.roundToTwoDecimals(semesterTotal / semesterCount);

                semesterTotal = semesterCount = 0;
                
                gradePointAverage = CollegeLife.roundToTwoDecimals(gradeTotal / gradeCount);
                
                if (gradePointAverage < 2.0)
                {
                    game.deansOffice(this, isOnAcademicProbation);
                }
                
            } else
            {
                gradeTotal += grade[i];
                gradeCount++;

                semesterTotal += grade[i];
                semesterCount++;
                
                semesterGrade = CollegeLife.roundToTwoDecimals(semesterTotal / semesterCount);

            }
        }

        
    }
}