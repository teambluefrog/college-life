package edu.uis.csc478.bluefrog;

import java.awt.*;
import java.io.*;
import java.net.URI;
import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.event.*;

@SuppressWarnings("serial")
/**
 * This class creates the GUI in which the player interacts with the College
 * Life Game.
 */
public class Window extends JFrame implements ActionListener
{
    /**
     * The four screens that make up the game.
     */
    public enum ScreenType
    {
        /**
         * Game Over screen seen at the end of play.
         */
        GAME_OVER, 
        
        /**
         * Game Overview screen seen if a user clicks the Game Overview on the Welcome (START) screen.
         */
        OVERVIEW, 
        
        /**
         * The Welcome screen - the first screen the player sees when opening the program.
         */
        START, 
        
        /**
         * The Turn screen - the screen in which the majority of the game is spent.
         */
        TURN
    };

    /**
     * The four font styles used in the game.
     */
    public enum FontStyle
    {
        /**
         * Futura Medium font.
         */
        MEDIUM, 
        
        /**
         * Futura Medium Condensed font.
         */
        CONDENSED_MEDIUM, 
        
        /**
         * Futura Bold font.
         */
        BOLD, 
        
        /**
         * Futura Condensed Extra Bold font.
         */
        CONDENSED_EXTRA_BOLD
    };

    /**
     * Color - dark blue.
     */
    public static final Color DARK_BLUE = new Color(52, 78, 93);
    
    /**
     * Color - light green
     */
    public static final Color LIGHT_GREEN = new Color(229, 238, 236); //TODO Update Style #E5EEEC
    
    /**
     * Color - orange
     */
    public static final Color ORANGE = new Color(225, 122, 63);
    
    /**
     * The height of the game window.
     */
    final int WINDOW_HEIGHT = 640;

    /**
     * The width of the game window.
     */
    final int WINDOW_WIDTH = 800;

    /**
     * The size of the screen
     */
    private Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();

    /**
     * The Game instance.
     */
    private Game game;
    
    /**
     * An instance of the Welcome screen.
     */
    private Screen_Start startScreen;
    
    /**
     * An instance of the Turn screen.
     */
    private Screen_Turn turnScreen;
    
    /**
     * An instance of the Game Over screen.
     */
    private Screen_GameOver gameOverScreen;
    
    /**
     * An instance of the Overview screen.
     */
    private Screen_Overview overviewScreen;
    
    /**
     * Instance of the Futura Medium font.
     */
    private Font fontMed;
    
    /**
     * Instance of the Futura Condensed Medium font.
     */
    private Font fontConMed;
    
    /**
     * Instance of the Futura Bold font.
     */
    private Font fontBold;
    
    /**
     * Instance of the Futura Condensed Extra Bold font.
     */
    private Font fontConXBold;
    
    /**
     * Image background for the Welcome screen.
     */
    private Image startBackground;
    
    /**
     * Image background for the Turn screen.
     */
    private Image turnBackground;
    
    /**
     * Image background for the Game Over screen.
     */
    private Image gameOverBackground;
    
    /**
     * Image background for the Overview screen.
     */
    private Image overviewBackground;
    
    /**
     * An array that holds the seven mood icons.
     */
    private ImageIcon mood[];
    
    /**
     * The game logo;
     */
    private Image logo;
    
    /**
     * URL to the user manual.
     */
    private URI manualURL;
    
    /**
     * STUDY button on turn screen.
     */
    public JButton studyButton;
    
    /** 
     * WORK button on turn screen.
     */
    public JButton workButton;
    
    /**
     * PLAY button on turn screen.
     */
    public JButton playButton;
    
    /**
     * Menu item for user manual.
     */
    public JMenuItem manual;
    
    /**
     * Menu item for new game.
     */
    public JMenuItem restart;

    /**
     * The Window constructor.
     * 
     * Sets initial state of display blocks in window.
     * 
     * @param game
     *            An initialized game object which holds all the game info such as
     *            turn, semester, and year and initializes the game and advances the
     *            turn.
     *            
     * RQ 3.9.1.1, RQ 3.9.1.2, RQ 3.9.2.2
     */
    public Window(Game game)
    {
        super("College Life");

        this.setLocation((screenSize.width / 2 - WINDOW_WIDTH / 2), (screenSize.height / 2 - WINDOW_HEIGHT / 2));

        this.game = game;
        
        loadResources();

        gameOverScreen = new Screen_GameOver(game, this, gameOverBackground);

        overviewScreen = new Screen_Overview(game, this, overviewBackground);

        startScreen = new Screen_Start(game, this, startBackground);

        turnScreen = new Screen_Turn(game, this, turnBackground, mood);

        setSize(WINDOW_WIDTH, WINDOW_HEIGHT);
        setResizable(false);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        // Menu bar for easily restarting the game during development.
        JMenuBar menuBar = new JMenuBar();
        JMenu gameMenu = new JMenu("Game");
        JMenu helpMenu = new JMenu("Help"); // Legacy code for User Manual
        restart = new JMenuItem("New Game");
        manual = new JMenuItem("User Manual");

        setJMenuBar(menuBar);
        menuBar.add(gameMenu);
        menuBar.add(helpMenu); // Legacy code for User Manual
        gameMenu.add(restart);
        restart.addActionListener(this);
        helpMenu.add(manual); // Legacy code for User Manual
        manual.addActionListener(this); // Legacy code for User Manual

        setContentPane(startScreen);
    }

    /**
     * Updates player's attributes based on the button that was clicked.
     */
    @Override
    public void actionPerformed(ActionEvent e)
    {
        Object source = e.getSource();

        if (source.equals(manual))
        {
            try
            {
                Desktop.getDesktop().browse(manualURL);
            } catch (Exception e1)
            {
                e1.printStackTrace();
            }
        }
        if (source.equals(restart))
        {
            game.restart();
        }
    }

    /**
     * Sets the active screen.
     * 
     * @param screen 
     *              The screen that gets displayed to the user.
     */
    public void changeScreen(ScreenType screen)
    {
        switch (screen)
        {
        case GAME_OVER:
            setContentPane(gameOverScreen);
            break;
        case OVERVIEW:
            setContentPane(overviewScreen);
            break;
        case START:
            setContentPane(startScreen);
            break;
        case TURN:
            setContentPane(turnScreen);
            break;
        }
        revalidate();
        repaint();
    }

    public void endGame(String reasonGameEnded)
    {
        gameOverScreen.displayGameOver(reasonGameEnded);
        changeScreen(ScreenType.GAME_OVER);
    }

    /**
     * Getter for font.
     * 
     * @param style 
     *              The font style - MEDIUM, CONDENSED_MEDIUM, BOLD, CONDENSED_EXTRA_BOLD
     * @return The font
     */
    public Font getFont(FontStyle style)
    {
        Font f = null;
        
        switch(style)
        {
            case MEDIUM:
                f = fontMed;
                break;
            case CONDENSED_MEDIUM:
                f = fontConMed;
                break;
            case BOLD:
                f = fontBold;
                break;
            case CONDENSED_EXTRA_BOLD:
                f = fontConXBold;
        }

        return f;
    }

    /**
     * Getter for turn screen.
     * 
     * @return Turn screen
     */
    public Screen_Turn getTurnScreen()
    {
        return turnScreen;
    }
    
    /**
     * Gets the url for the user manual.
     * 
     * @return The web address of the user manual
     */
    public URI getUserManual()
    {
        return manualURL;
    }

    /**
     * Initializes the display of the game.
     */
    private void loadResources()
    {
        try
        {
            InputStream tempStream = null;
            tempStream = Window.class.getResourceAsStream("Futura-Medium.ttf");
            fontMed = Font.createFont(Font.TRUETYPE_FONT, tempStream);
            tempStream = Window.class.getResourceAsStream("Futura-CondensedMedium.ttf");
            fontConMed = Font.createFont(Font.TRUETYPE_FONT, tempStream);
            tempStream = Window.class.getResourceAsStream("Futura Bold.ttf");
            fontBold = Font.createFont(Font.TRUETYPE_FONT, tempStream);
            tempStream = Window.class.getResourceAsStream("futura-condensed-extra-bold-regular-1361540891.ttf");
            fontConXBold = Font.createFont(Font.TRUETYPE_FONT, tempStream);
            
        } catch (Exception e)
        {
            System.out.println("Error loading font: " + e.getMessage());
        }
        
        try
        {
            InputStream tempStream = null;
            tempStream = Window.class.getResourceAsStream("bgWelcome.png");
            startBackground = ImageIO.read(tempStream);
            tempStream = Window.class.getResourceAsStream("bgTurn.png");
            turnBackground = ImageIO.read(tempStream);
            tempStream = Window.class.getResourceAsStream("bgGameOver.png");
            gameOverBackground = ImageIO.read(tempStream);
            tempStream = Window.class.getResourceAsStream("bgOverview.png");
            overviewBackground = ImageIO.read(tempStream);
 
        } catch (Exception e)
        {
            System.out.println("Error loading background image: " + e.getMessage());
        }
        
        try
        {
            // Mood indicator icons.
            mood = new ImageIcon[7];
            
            InputStream tempStream = null;
            
            tempStream = Window.class.getResourceAsStream("smiley-3-small.png");
            mood[0] = new ImageIcon(ImageIO.read(tempStream));
            tempStream = Window.class.getResourceAsStream("smiley-2-small.png");
            mood[1] = new ImageIcon(ImageIO.read(tempStream));
            tempStream = Window.class.getResourceAsStream("smiley-1-small.png");
            mood[2] = new ImageIcon(ImageIO.read(tempStream));
            tempStream = Window.class.getResourceAsStream("smiley0-small.png");
            mood[3] = new ImageIcon(ImageIO.read(tempStream));
            tempStream = Window.class.getResourceAsStream("smiley1-small.png");
            mood[4] = new ImageIcon(ImageIO.read(tempStream));
            tempStream = Window.class.getResourceAsStream("smiley2-small.png");
            mood[5] = new ImageIcon(ImageIO.read(tempStream));
            tempStream = Window.class.getResourceAsStream("smiley3-small.png");
            mood[6] = new ImageIcon(ImageIO.read(tempStream));

        } catch (IOException e)
        {
            System.out.println("Error loading smiley icons: " + e.getMessage());
        }
        
        try
        {
            manualURL = new URI("http://bluefrogdevelopment.com/college-life/manual.pdf");
        } catch (Exception e)
        {
            System.out.println("Error loading user manual URL: " + e.getMessage());
        }
        
        try
        {
            InputStream tempStream = null;
            tempStream = Window.class.getResourceAsStream("logo.png");
            logo = ImageIO.read(tempStream);
            this.setIconImage(logo);;
            
        } catch (Exception e)
        {
            System.out.println("Error loading logo icon: " + e.getMessage());
        }
    }

}
