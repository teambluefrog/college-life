package edu.uis.csc478.bluefrog;

import java.util.Random;

import javax.swing.JOptionPane;

/**
 * This class starts a game, ends a game, and advances the player's turns in the
 * game.
 */
public class Game
{
    /**
     * The window that holds the game.
     */
    public Window mainWindow;

    /**
     * The user playing the game.
     */
    public Student player;

    /**
     * A random life event that may occur during play.
     */
    public Event event;

    /**
     * The current index (starting at zero) for the turn the player is on.
     */
    private int turnIndex;

    /**
     * The semester and year of the player's current turn.
     */
    private String semester, year;

    /**
     * String that holds the notification to the player they they received a loan.
     */
    private String coaReceipt;

    /**
     * If a life event has happened during a turn, this holds the title of the
     * event.
     */
    private String turnEvent;

    /**
     * If a life event has happened during a turn, this holds the details of the
     * event.
     */
    private String turnContent;

    /**
     * Random Number Generator to generate decimal between 0 and 1.
     */
    private Random rng = new Random();

    /**
     * The Game constructor sets the game in motion, initializing the window the
     * game will be played in, the player, and the turn.
     */
    public Game()
    {
        mainWindow = new Window(this);
        mainWindow.setVisible(true);
        player = new Student(this);
        initialize();
        beginTurn();
    }

    /**
     * The player is notified about what has taken place as a result of their button
     * choice and a life event that may have occurred. This method is used to add
     * the lines of text into a single string that then gets output to the UI.
     * 
     * @param messageToAdd
     *            The message to be added to the string that eventually gets
     *            displayed in the UI.
     */
    public void appendToTurnContent(String messageToAdd)
    {
        turnContent += ("\n\n" + messageToAdd);
    }

    /**
     * Increments the turn by one, sets the semester and year of the new turn, and
     * displays the semester and year to the user in the game window.
     * 
     * RQ 3.1.1, RQ 3.1.2
     */
    public void beginTurn()
    {
        if (turnIndex % CollegeLife.TURNS_PER_SEMESTER == 0)
        {
            switch (turnIndex / CollegeLife.TURNS_PER_SEMESTER % CollegeLife.SEMESTERS_PER_YEAR) // Calculate index of
                                                                                                 // current semester
            {
            case 0: // First turn of Fall Semester, New Year
                semester = "Fall";
                switch (turnIndex / CollegeLife.TURNS_PER_YEAR)
                {
                case 0:
                    year = "Freshman";
                    break;
                case 1:
                    year = "Sophomore";
                    break;
                case 2:
                    year = "Junior";
                    break;
                case 3:
                    year = "Senior";
                    break;
                default:
                    System.out.println("Turn is out of range");
                    break;
                }
                break;
            case 1: // First turn of Spring Semester
                semester = "Spring";
                break;
            default:
                System.out.println("Semester is out of range");
                break;
            }
        }

        // Resets turn-specific additional information
        coaReceipt = "";
        turnEvent = turnContent = "";

        mainWindow.getTurnScreen().displayCurrentTurn(turnIndex);
    }

    /**
     * If a player has enough money to pay for cost of attendance, the money is
     * taken out of a player's funds. If a player does not have enough money and has
     * not reached the debt limit, the player can request a loan. Otherwise, the
     * player runs out of money and game ends.
     * 
     * @param receivedLoanOffer
     *              Whether the player has received a loan in order to pay COA.  
     *              
     * RQ 3.5.4, RQ 3.6.3, RQ 3.6.8, RQ 3.8.3
     */
    public void payCostOfAttendance(boolean receivedLoanOffer)
    {
        if (player.getFunds() >= player.getCOA())
        {
            player.increaseFunds(player.getCOA() * (-1));
        }
        else if (player.getDebt() < CollegeLife.DEBT_LIMIT && !receivedLoanOffer)
        {
            player.requestLoan(true);
            payCostOfAttendance(true);
        }
        else
        {
            endGame("You" + (char) 39 + "ve run out of money and can't pay for the semester!");
        }
    }

    /**
     * Adjustments are made to the player's attributes according to the button
     * they've pressed during the turn.
     * 
     * @param focus
     *            The button that was pressed.
     *            
     * RQ 3.2.2, RQ 3.2.4, RQ 3.2.5, RQ 3.2.6, RQ 3.2.7, RQ 3.2.8, RQ 3.2.9, RQ 3.6.5
     */
    public void chooseFocus(CollegeLife.Focus focus)
    {
        switch (focus)
        {
        case STUDY:
            player.setFocus(CollegeLife.Focus.STUDY);
            player.increaseGrade(CollegeLife.STUDY_INCREMENT * 1);
            player.increaseFunds(CollegeLife.WORK_INCREMENT * 0);
            player.increaseMood(CollegeLife.PLAY_INCREMENT * 0);
            resolveTurn();
            break;
        case WORK:
            player.setFocus(CollegeLife.Focus.WORK);
            player.increaseGrade(CollegeLife.STUDY_INCREMENT * 0);
            if (player.getIsEmployed())
            {
                player.increaseFunds(CollegeLife.WORK_INCREMENT * 1);
            } else
            {
                player.increaseFunds(CollegeLife.WORK_INCREMENT * 0.5);
            }
            player.increaseMood(CollegeLife.PLAY_INCREMENT * (-1));
            resolveTurn();
            break;
        case PLAY:
            player.setFocus(CollegeLife.Focus.PLAY);
            player.increaseGrade(CollegeLife.STUDY_INCREMENT * (-0.5));
            double priceOfAdmission = CollegeLife.WORK_INCREMENT * (0.25);
            if (player.getFunds() < priceOfAdmission)
            {
                player.requestLoan(false);
            }
            player.increaseFunds(priceOfAdmission * (-1));
            player.increaseMood(CollegeLife.PLAY_INCREMENT * 1);
            resolveTurn();
            break;
        }
    }

    /**
     * If a player's GPA drops below 2.0 or they were caught cheating, they are sent
     * to the Dean's office and placed on academic probation.
     * 
     * @param s
     *            The player or student
     * @param isOnProbation
     *            Whether the player was already on academic probation. If so, game
     *            ends.
     *            
     * RQ 3.3.1.4, RQ 3.8.2
     */
    public void deansOffice(Student s, boolean isOnProbation)
    {
        if(isOnProbation)
        {
            if(player.getGPA() < 2.0)
            {
                endGame("Your GPA is too low. Maintain a 2.0 next time!");
            } else {
                endGame("You cheated while on academic probation!");
            }
        }
        else
        {
            JOptionPane.showMessageDialog(mainWindow,
                    "Your GPA is below 2.0 and you are in danger\n"
                    + "of being kicked out of school.\n"
                    + "\nYou are being placed on academic probation.",
                    "Warning from the Dean",
                    JOptionPane.WARNING_MESSAGE);
            s.placeOnProbation();
        }
    }

    /**
     * Results of a players turn get displayed on the UI.
     */
    public void displayResults()
    {
        mainWindow.getTurnScreen().displayResults(turnIndex, player.getFocus(),
                player.getEffects(CollegeLife.Focus.PLAY), player.getEffects(CollegeLife.Focus.STUDY),
                player.getEffects(CollegeLife.Focus.WORK), coaReceipt, turnEvent, turnContent);
    }

    /**
     * Game has ended.
     * 
     * @param reason
     *            The reason the game has ended.
     *            
     * RQ 3.8.4, RQ 3.8.5
     */
    public void endGame(String reason)
    {
        JOptionPane.showMessageDialog(mainWindow, reason, "Game Over", JOptionPane.ERROR_MESSAGE);
        mainWindow.endGame(reason);
    }

    /**
     * Gets the semester of the current turn.
     * 
     * @return Fall or Spring
     */
    public String getSemester()
    {
        return semester;
    }

    /**
     * Gets the player's current turn.
     * 
     * @return An integer indicating the current turn.
     */
    public int getTurnIndex()
    {
        return turnIndex;
    }

    /**
     * Gets the year of the current turn.
     * 
     * @return Freshman, Sophomore, Junior, or Senior
     */
    public String getYear()
    {
        return year;
    }

    /**
     * Creates a random grade and calculates new semester grade.
     * 
     * RQ 3.3.2.2, RQ 3.3.2.3, RQ 3.3.2.4, RQ 3.3.2.5, RQ 3.3.2.6
     */
    public void gradeTurn()
    {
        double newGrade;

        // First turn of the semester uses the default mean.
        if ((turnIndex % CollegeLife.TURNS_PER_SEMESTER) == 0)
        {
            newGrade = (rng.nextGaussian() * CollegeLife.GRADE_STD_DEV) + CollegeLife.GRADE_MEAN;
        } 
        // Successive turns use prior turn's grade as mean.
        else 
        {
            newGrade = (rng.nextGaussian() * CollegeLife.GRADE_STD_DEV) + player.getGrade(turnIndex - 1);
        }

        newGrade += player.getGradeBonus();
        // Once grade bonus is applied, it's reset to zero
        player.resetGradeBonus(); 

        newGrade = CollegeLife.roundToTwoDecimals(newGrade);

        if (newGrade > 4.0)
        {
            newGrade = 4.0;
        }
        else if (newGrade < 0)
        {
            newGrade = 0;
        }

        player.setGrade(turnIndex, newGrade);
    }

    /**
     * Kicks off game play by initializing the player, window, and sets the current
     * turn to 0.
     */
    public void initialize()
    {
        turnIndex = 0;
        player.initialize();
        mainWindow.getTurnScreen().initializeDisplay();
    }

    /**
     * Kicks off the mechanics for each turn - generates random event, pays COA if second turn
     * of semester, generates, grade, updates player status.
     * 
     * RQ 3.7.1
     */
    public void resolveTurn()
    {
        int randomEvent = rng.nextInt(Event.getEventChanceLimit());
        event = new Event(this, player, randomEvent);

        if (turnIndex % CollegeLife.TURNS_PER_SEMESTER == 0)
        {
            payCostOfAttendance(false);
        } else
        {
            player.checkFunds();
        }
        gradeTurn();
        player.checkEmployment();
        player.calculateStatusChange(turnIndex);
        displayResults();

        turnIndex++;

        if (turnIndex >= CollegeLife.MAX_TURNS)
        {
            endGame("You successfully graduated with a " + player.getGPA() + " GPA and " + CollegeLife.formatMoney(player.getDebt()) + " in debt.");
        } else
        {
            beginTurn();
        }
    }

    /**
     * Initializes a new game.
     */
    public void restart()
    {
        initialize();
        beginTurn();
        mainWindow.changeScreen(Window.ScreenType.TURN);
    }

    /**
     * Sets the title of a life event.
     * 
     * @param eventTitle
     *            The title of the event.
     */
    public void setTurnEvent(String eventTitle)
    {
        turnEvent += (eventTitle + "\n");
    }

}