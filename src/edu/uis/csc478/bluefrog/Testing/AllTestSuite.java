package edu.uis.csc478.bluefrog.Testing;

import org.junit.runners.Suite;
import org.junit.runner.RunWith;

@RunWith(Suite.class)
@Suite.SuiteClasses({CollegeLifeTest.class, GameTest.class, StudentTest.class})
public class AllTestSuite {
  //nothing
}