package edu.uis.csc478.bluefrog.Testing;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;

import org.junit.Test;

import edu.uis.csc478.bluefrog.CollegeLife;

import edu.uis.csc478.bluefrog.Game;
import edu.uis.csc478.bluefrog.Student;

import static org.junit.Assert.*;


@SuppressWarnings("unused")
public class GameTest {
	Game game=new Game();
	
	Student s = new Student(game);

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	     
	}
	@AfterClass
	public static void tearDownClass() throws Exception {
		System.out.println("Game Testing Complete");
	}

	@Test
	public void testBeginTurn() {
	System.out.println("testing beginTurn");
	int min = 0;
	int max = 32;
	String year1 = "Freshman";
	String year2 = "Sophomore";
	String year3 = "Junior";
	String year4 = "Senior";
	String semester1 = "Fall";
	String semester2 = "Spring";

		game.beginTurn();
	assertTrue("Incorrect turn value.", game.getTurnIndex()>= min && game.getTurnIndex()<= max);;
	System.out.println(game.getTurnIndex());	
	assertTrue("Incorrect Year",game.getYear().equals(year1) || game.getYear().equals(year2) || game.getYear().equals(year3) || game.getYear().equals(year4));
	 System.out.println(game.getYear());
	 assertTrue("Incorrect Semester", game.getSemester().equals(semester1) || game.getSemester().equals(semester2));
	 System.out.println(game.getSemester());
	}

		
		@Test
		public void testPayCostOfAttendance(){
			System.out.println("Run @Test payCostOfAttendance");
			System.out.println("Funds before payment: " + game.player.getFunds());
			game.payCostOfAttendance(true);
			int actual_fundsAfterPayment = game.player.getFunds();
			System.out.println("Funds after payment: " + actual_fundsAfterPayment); 
			
		}
	
	@Test
	public void testInitialize() {
		System.out.println("testing Initialize");
		int expected_starting_turn = 0;
				game.initialize();			
		int actual_starting_turn = game.getTurnIndex(); // adding or subtracting to game.getTurn() will cause test to fail
		System.out.println(actual_starting_turn);
		assertTrue("Not a new game. Turn is: " + actual_starting_turn, actual_starting_turn == expected_starting_turn); //first turn in game is 1. For a game to be initialize, it would be turn 0 to start.
	}
	
	@Test
	public void testRestart() {
		System.out.println("testing restart");
		int expected_starting_turn = 0;
				game.restart();			
		int actual_starting_turn = game.getTurnIndex();
		System.out.println(actual_starting_turn);
		assertTrue("Game not restarted. Turn is: " + actual_starting_turn, actual_starting_turn == expected_starting_turn); //first turn in game is 1. For a game to be initialize, it would be turn 0 to start.
	
	}
	
	
}
