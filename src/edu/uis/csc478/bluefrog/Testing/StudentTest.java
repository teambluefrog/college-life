package edu.uis.csc478.bluefrog.Testing;

import static org.junit.Assert.*;


import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import edu.uis.csc478.bluefrog.CollegeLife;
import edu.uis.csc478.bluefrog.Game;
import edu.uis.csc478.bluefrog.Student;


public class StudentTest {
	Student student=new Student(new Game());
	CollegeLife cl=new CollegeLife();
	enum test{
		FINAL,SOCIAL
	}
	
	@Before
	public void setUp() throws Exception {
	}
	CollegeLife.Focus f;
	@After
	public void tearDown() throws Exception {
		
	}
	@AfterClass
	public static void tearDownClass() throws Exception {
		
		System.out.println(" Student Testing Complete");
	}
	
	@Test
	 
    public void testIncreaseCOA(){
		System.out.println("Run @Test increaseCOA");
		double expect_COA = 7000.0;
		System.out.println(student.getCOA());
		student.increaseCOA(2000);
		System.out.println(student.getCOA());
		double actual_COA = student.getCOA();
		Assert.assertTrue("Invalid input", expect_COA == actual_COA);
		
	}
	@Test
	 
    public void testIncreaseFunds(){
		System.out.println("Run @Test increaseFunds");
		double expect_Funds = 9000;
		System.out.println("Original funds: " +student.getFunds());
		student.increaseFunds(2000);
		System.out.println("Funds after increase: " +student.getFunds());
		double actual_Funds = student.getFunds();
		Assert.assertTrue("Invalid input", expect_Funds == actual_Funds);
		
	}
	@Test
	 
    public void testIncreaseGrade(){
		System.out.println("Run @Test increaseGrade");
		
		double expected_gradeIncrease = 1.5;
		student.increaseGrade(1.5);
		double actual_gradeIncrease = student.getGradeBonus();
		System.out.println(student.getGradeBonus());
		Assert.assertTrue("Invalid grade increase.", expected_gradeIncrease == actual_gradeIncrease);
		
	}
	@Test
	 
    public void testIncreaseMood(){
		System.out.println("Run @Test increaseMood");
		double expect_Mood = 1;
		System.out.println(student.getMood());
		student.increaseMood(1);
		System.out.println(student.getMood());
		double actual_Mood = student.getMood();
		Assert.assertTrue("Invalid input", expect_Mood == actual_Mood);
		
	}
	@Test
	public void testingeRequestLoan() {
		double expected_debt = 6250;
		student.requestLoan(true);
		double actual_debt = student.getDebt();
		System.out.println(student.getDebt());
		assertTrue("Loan invalid", actual_debt == expected_debt);
	}


	
	@Test
	  public void testCalculateStatusChange() {
	
	
		student.calculateStatusChange(1);
		assertTrue(" Incorrect Grade update.", student.getGrade(1) >= 0 || student.getGrade(1) <= 0);
		System.out.println(student.getGrade(1));	
		assertTrue("Mood error",student.getMood() <=3 || student.getMood() >=-3);
		 System.out.println(student.getMood());
		 assertTrue("Funds error",  student.getFunds() >= 0);
		 System.out.println(student.getFunds());
		
	}
	
}
	
	

