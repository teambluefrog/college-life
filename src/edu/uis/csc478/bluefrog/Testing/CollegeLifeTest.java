package edu.uis.csc478.bluefrog.Testing;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;


import edu.uis.csc478.bluefrog.CollegeLife;

public class CollegeLifeTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		System.out.println("CollegeLife Testing Complete");
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}
	
	@Test
	public void testRoundToTwoDecimals() {
		System.out.println("Run @Test roundToTwoDecimals"); // for illustration
		double expected_rounding = 3.15;
		double actual_rounding = CollegeLife.roundToTwoDecimals(3.1454);
		Assert.assertTrue("Rounding failed", expected_rounding == actual_rounding);
		
	}
	@Test
	public void testCompareLetterGrades() {
		System.out.println("Testing compareLetterGrades"); 
		@SuppressWarnings("unused")
		int expected = -1;
		int actual = CollegeLife.compareLetterGrades("A","A-");
		System.out.println(actual);
		Assert.assertTrue("Grade Comparision Failed", expected == actual);
	}	
	@Test
	public void testConvertToLetterGrade() {
		System.out.println("Testing convertToLetterGrade");
		String expected ="B-";
		String actual = CollegeLife.convertToLetterGrade(2.7);
		System.out.println(actual);
		Assert.assertEquals("Letter Grade Conversion Failed", expected, actual);
	}
	
	@Test
	public void testFormatMoney() {
		System.out.println("Run @Test formatMoney"); // for illustration
		String expected_format = "$7,000";
		System.out.println(expected_format);
		String actual_format = CollegeLife.formatMoney(7000);
		System.out.println(actual_format);
		Assert.assertEquals("Money Formatting failed", expected_format, actual_format);
		
	}
	
	
}

