package edu.uis.csc478.bluefrog;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

/**
 * This class contains the GUI for the Welcome screen.
 * 
 * RQ 3.9.2.1, RQ 3.9.2.3, RQ 3.9.2.4
 */
@SuppressWarnings("serial")
public class Screen_Start extends JPanel implements ActionListener
{
    @SuppressWarnings("unused")
    private Game game;
    private Window window;
    private Image background;

    private JButton playButton, overviewButton;

    /**
     * Constructor - Adds components to screen for display.
     * 
     * @param game
     *            The Game instance.
     * 
     * @param window
     *            The Window instance.
     *            
     * @param background
     *            The background image for the window.
     */
    public Screen_Start(Game game, Window window, Image background)
    {
        this.game = game;
        this.window = window;
        this.background = background;

        setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        c.insets = new Insets(0, 0, 0, 0);

        // Adds PLAY button.
        playButton = new JButton("PLAY");
        playButton.addActionListener(this);
        playButton.setPreferredSize(new Dimension(210, 72));
        playButton.setFont(window.getFont(Window.FontStyle.CONDENSED_EXTRA_BOLD).deriveFont(34f));
        playButton.setForeground(Color.WHITE);
        playButton.setBackground(Window.ORANGE);
        playButton.setOpaque(true);
        playButton.setBorder(BorderFactory.createRaisedBevelBorder());
        c.gridx = 0;
        c.gridy = 0;
        c.insets.set(290, 20, 0, 0);
        add(playButton, c);

        // Adds Game Overview button.
        overviewButton = new JButton("Game Overview");
        overviewButton.addActionListener(this);
        overviewButton.setPreferredSize(new Dimension(176, 36));
        overviewButton.setFont(window.getFont(Window.FontStyle.CONDENSED_MEDIUM).deriveFont(19f));
        overviewButton.setForeground(Window.DARK_BLUE);
        overviewButton.setBackground(Color.WHITE);
        overviewButton.setOpaque(true);
        overviewButton.setBorder(BorderFactory.createLineBorder(Window.DARK_BLUE, 2));
        c.gridx = 0;
        c.gridy = 1;
        c.insets.set(130, 0, 0, 560);
        add(overviewButton, c);
    }

    /**
     * Sets button functionality.
     */
    @Override
    public void actionPerformed(ActionEvent event)
    {
        Object source = event.getSource();

        if (source.equals(playButton))
        {
            window.changeScreen(Window.ScreenType.TURN);
        }
        if (source.equals(overviewButton))
        {
            window.changeScreen(Window.ScreenType.OVERVIEW);
        }
    }

    /*
     * Original Code by Michael Myers via Stack Overflow
     * 
     * stackoverflow.com/users/13531/michael-myers
     */
    @Override
    protected void paintComponent(Graphics g)
    {
        super.paintComponent(g);
        g.drawImage(background, 0, 0, this);
    }

}
