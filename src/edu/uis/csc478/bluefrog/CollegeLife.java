package edu.uis.csc478.bluefrog;

import java.text.DecimalFormat;

/**
 * The College Life class provides game defaults and some utility conversion
 * methods for grade and number display.
 */
public class CollegeLife
{
    /**
     * The number of turns per semester is {@value}.
     */
    public static final int TURNS_PER_SEMESTER = 4;

    /**
     * The number of semesters per year is {@value}.
     */
    public static final int SEMESTERS_PER_YEAR = 2;

    /**
     * The number of years in a game is {@value}.
     */
    public static final int MAX_YEARS = 4;

    /**
     * The number of turns that make up a year of college.
     */
    public static final int TURNS_PER_YEAR = TURNS_PER_SEMESTER * SEMESTERS_PER_YEAR;

    /**
     * The total number of turns that make up the game for the player.
     */
    public static final int MAX_TURNS = (TURNS_PER_YEAR * MAX_YEARS);

    /**
     * The turn number of the first fall turn is {@value}.
     */
    public static final int FALL_START_TURN = 0;

    /**
     * The turn number of the first spring turn.
     */
    public static final int SPRING_START_TURN = TURNS_PER_YEAR / 2;

    /**
     * Each item represents a button during game play as an option for the player to
     * focus their attention during each turn.
     */
    public enum Focus
    {
        /**
         * STUDY button on Turn screen.
         */
        STUDY,

        /**
         * WORK button on Turn screen.
         */
        WORK,

        /**
         * PLAY button on Turn screen.
         */
        PLAY
    };

    /**
     * The player begins the game with a starting GPA of {@value}.
     */
    public static final double STARTING_GPA = 0.0;

    /**
     * The player begins the game with ${@value}.
     * 
     * RQ 3.5.3
     */
    public static final int STARTING_FUNDS = 7000;

    /**
     * The cost of attending each semester is ${@value}.
     * 
     * RQ 3.5.2
     */
    public static final int COST_OF_ATTENDANCE = 5000; // per semester

    /**
     * The max amount of debt a player is allowed per game is ${@value}.
     * 
     * R.Q. 3.6.4
     */
    public static final int DEBT_LIMIT = 25000;

    /**
     * The amount of a student loan ${@value}. A player can take out up to 4 student
     * loans per game.
     * 
     * RQ 3.6.1
     */
    public static final int LOAN_AMOUNT = 6250;

    /**
     * The player begins the game with a mood of {@value}.
     * 
     * RQ 3.4.2
     */
    public static final int STARTING_MOOD = 0;

    /**
     * Player's GPA is calculated on a bell curve with a mean of {@value}.
     */
    public static final double GRADE_MEAN = 2.7;

    /**
     * Player's GPA is calculated on a bell curve with a standard deviation of
     * {@value}.
     */
    public static final double GRADE_STD_DEV = 0.5;

    /**
     * The finals grade is given a weight of {@value}.
     */
    public static final double FINALS_WEIGHT = 2.0;

    /**
     * The default amount that the player's study attribute will change is {@value}.
     */
    public static final double STUDY_INCREMENT = 0.5;
    /**
     * The default amount that the player's finances attribute will change is
     * {@value}.
     */
    public static final int WORK_INCREMENT = 1000;
    /**
     * The default amount that the player's mood attribute will change is {@value}.
     */
    public static final int PLAY_INCREMENT = 1;

    /**
     * The journey starts here.
     * 
     * @param args
     *            Not used in this program.
     */
    public static void main(String[] args)
    {
        try
        {
            @SuppressWarnings("unused")
            Game game = new Game();
        } catch (Exception e)
        {
            System.out.println(e.getMessage());
        }

    }

    /**
     * Utility function for comparing letter grades.
     * 
     * @param first
     *            First grade to compare.
     * @param second
     *            Second grade to compare.
     * 
     * @return Returns a 0 if the grades are equal. Returns an integer value less
     *         than 0 if the argument is a string lexicographically greater than
     *         this string. Returns an integer value greater than 0 if the argument
     *         is a string lexicographically less than this string.
     */

    public static int compareLetterGrades(String first, String second)
    {
        /*
         * String.CompareTo() treats A higher than A+ (i.e. A > A+ > A-)
         * 
         * If grade has no +/-, this appends a ',' before comparison (e.g. "A" becomes
         * "A,")
         * 
         * This establishes A+ > A, > A-
         */

        String grade1 = first;
        String grade2 = second;

        if (grade1.length() == 1)
        {
            grade1 += ",";
        }
        if (grade2.length() == 1)
        {
            grade2 += ",";
        }

        return grade1.compareTo(grade2);
    }

    /**
     * Utility function that converts a point grade to a letter grade.
     * 
     * @param fourPointGrade
     *            Point grade to be converted to a letter grade.
     * 
     * @return A String letter grade with modifier (+,-).
     */
    public static String convertToLetterGrade(double fourPointGrade)
    {
        String grade = null;

        int letterDeterminant = (int) (fourPointGrade + 0.5);
        double modifierDeterminant = (fourPointGrade % 1);

        switch (letterDeterminant)
        {
        case 4:
            grade = "A";
            break;
        case 3:
            grade = "B";
            break;
        case 2:
            grade = "C";
            break;
        case 1:
            grade = "D";
            break;
        default:
            grade = "F";
        }

        if (letterDeterminant < 4 && modifierDeterminant < 0.5 && modifierDeterminant >= 0.15)
        {
            grade += "+";
        } else if (modifierDeterminant >= 0.5 && modifierDeterminant < 0.85)
        {
            grade += "-";
        }

        return grade;
    }

    /**
     * Formats a dollar amount.
     * 
     * @param money
     *            The number to format.
     * 
     * @return Returns a String in the format of $#,###,###.
     */
    public static String formatMoney(double money)
    {
        DecimalFormat moneyFormat = new DecimalFormat("#,###,###");
        return "$" + moneyFormat.format(money);
    }

    /**
     * Utility method used to round a double to two decimal places.
     * 
     * @param numberToRound
     *            The number to round to two decimal places.
     * 
     * @return The number after it's been rounded to two decimal places.
     */
    public static double roundToTwoDecimals(double numberToRound)
    {
        double temp = numberToRound;
        temp *= 100;
        temp += 0.5;
        temp = (int) temp;
        temp /= 100;

        return temp;
    }
}
