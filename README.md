[TOC]

# Quick Summary

This is a repo for the CSC 478 Software Engineering Capstone class.  It is a Java project for a game called College Life.  College Life is a merging of the game Monopoly with the video game from the 90s, The Oregon Trail.

# Who do I talk to?

* Repo admin - Michelle Williamson
* Team members - Jayson Howell, Alex Powell

# Using Git

## Git Setup

The instructions in this section are a one-time event.  Once you complete the Git Setup section, you won't have to do it again unless you change computers. 

1. Download Git from https://git-scm.com/downloads and install.

1. Create an account with your UIS email on Bitbucket.org. I've added you as team members on Bitbucket using your UIS email so your new account "should" give you access to the repository.  We'll see if that works as expected.

1. Generate an SSH key and add it to Bitbucket.  Expand the "Set up SSH for Git on Windows" section at https://confluence.atlassian.com/bitbucket/set-up-an-ssh-key-728138079.html. Note: If you already have an existing key, you can use that - skip step 1 in the instructions and proceed to step 2 of the Bitbucket instructions for generating a key.

1. Add your details to git.  From the command line, do:   

    `git config --global user.name "John Doe"`
    
    `git config --global user.email johndoe@example.com`

1. Clone the repository from Bitbucket:

    `git clone <Bitbucket Username>@bitbucket.org:teambluefrog/college-life.git`


## Git Workflow

These are the commands you'll be using whenever you're coding.

Note: Develop branch - never commit changes directly to this branch.  All of your code changes and updates will be done in a feature branch

### Creating a feature branch

1. cd to project folder

    `cd path/to/project/folder`

2. Check out the develop branch.  This is the branch you'll be creating your feature branch from

    `git checkout develop`

3. Fetch any changes that were merged into the develop branch on Bitbucket

    `git fetch origin`

4. Apply the changes to your local develop branch

    `git rebase origin/develop`

5. Create your feature branch where you will be adding and committing your additions and changes.

    `git checkout -b feature/[name]/[branch-name]`

    (text in brackets should be replaced with appropriate text)
    
    example: git checkout -b feature/michelle/adds-player-class
    
### Updating your feature branch

Sometimes while you're working and always before you push your feature branch to Bitbucket, you want to pull any changes that have been added to the develop branch into your feature branch.  A good rule of thumb is to do this any time you sit down and begin working. On an active project with many developers, you'll want to do this regularly - several times per day to several times per hour depending on the activity level of the project.

1. Update your local develop branch to match the develop branch on Bitbucket:

    `git fetch origin` // Fetches any changes made to the Bitbucket repo
    
    `git checkout develop` // Checks out your local develop branch 
    
    `git rebase origin/develop` // Writes the changes from the Bitbucket develop branch onto your local develop branch

2. Pull the changes from your local develop branch into your feature branch:

    `git checkout feature/[name]/[branch-name]` // Check out your local feature branch
    
    `git merge develop` // Merge the changes from your local develop branch into your local feature branch

### Resolving merge conflicts

Sometimes when two developers are working in the same area of code, Git won't know how to merge the two chunks of code and you'll need to do it manually.  Pay attention to Git's response when you do git rebase and git merge.  If there are merge conflicts, it will tell you.  You'll see something like this:

        Auto-merging test.html
        CONFLICT (content): Merge conflict in test.html
        Automatic merge failed; fix conflicts and then commit the result.

To resolve this, open up the file that has the merge conflict.  From the results above, it says there's a merger conflict in  test.html.  You'll see something like this:

        <<<<<<< HEAD
        2
        =======
        1
        >>>>>>> develop

What's between "<<<<<<<< HEAD and "=======" is what was in your feature branch.
What's between "========" and ">>>>>>> develop" was what you're trying to merge in. 

So this is saying, "Hey!  You have a 2 in your feature branch where there's a 1 in the develop branch.  We don't know whether to keep the 2, the 1, or both.  You figure it out for us."  

Delete everything except for the code you want to keep.  So in this case, we want to replace the 1 with the 2 so we delete the other 4 lines.

When you're sure you've kept what you needed and deleted anything else, you need to add and commit your change.

`git add [file]`

`git commit`  // A commit message will automatically be added so you don't need to use -m to add a message

Accept the message that's automatically added by typing:
ESC (press the escape key, don't actually type ESC!) followed by :wq

Don't forget to push your feature to Bitbucket if that's what you were doing before running into the merge conflict! 

### Saving your work and pushing to Bitbucket ####

When done working and ready to save your work to the feature branch:

1. See what files have been modified or added to the project:

    To see what files you've changed, do: 
    
    `git status`

    To see what you've changed in each file, do:
   
    `git diff [filename]`  

2. Adding your files to the repo is a two-step process.  First, you stage your files for commit:
    
    `git add [filename]`

    (Repeat this step for each file.)

3. Then you commit your changes to the branch:

    `git commit -m "[commit message]"`

    (Text in brackets should briefly describe the work you are committing.)

4. Push your branch up to Bitbucket so the rest of the team can see them and pull them down to their local:

    `git push origin feature/[name]/[branch-name`

5. Create a pull request by going to https://bitbucket.org/teambluefrog/college-life/pull-requests/new and choose your feature branch in the left hand dropdown and develop in the right hand dropdown.  Click "create pull request"

I will receive a message that a new pull request was created and will take it from there.  I will add my documentation and then merge it into develop.  Just make sure you always update develop before creating your feature branches.  (If you don't, it's not the end of the world.  Just holler and I'll walk you through updating after the fact.)


# Troubleshooting

* I'm getting "Error: Could not find or load main class edu.uis.csc478.bluefrog.CollegeLife" when I try to run the code. 

Clean the project and then run: File > Refresh

